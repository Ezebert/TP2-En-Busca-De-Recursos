package interfaz;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import acceso.LasMinas;
import acceso.LosGrafos;
import negocio.aplicar;
import negocio.dibujar;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class agregarArista extends JFrame {

	private static final long serialVersionUID = 1L;
	private JTextField txtDistancia;
	private JTextField txtVertice2;
	private JTextField txtVertice1;

	/**
	 * Create the frame.
	 */
	public agregarArista() {
		configuracionInicial();
		vertice1();
		vertice2();
		distancia();
		agregarAristas();

	}

	private void agregarAristas() {
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int i = LasMinas.buscarPosicion(Integer.parseInt(txtVertice1.getText()));
				int j = LasMinas.buscarPosicion(Integer.parseInt(txtVertice2.getText()));
				double peso = Double.parseDouble(txtDistancia.getText());
				// Ambos vertice ingresado por el usuario son identicos
				if (i == j)
					JOptionPane.showMessageDialog(null, "Deben ser diferentes (" + i + " " + j + ")");
				if (i < 0 || i >= LosGrafos.getGrafo().numVertices())
					JOptionPane.showMessageDialog(null,
							"Se intento procesar una " + "arista con un (1)vertice fuera de rango! i ");
				if (j < 0 || j >= LosGrafos.getGrafo().numVertices())
					JOptionPane.showMessageDialog(null,
							"Se intento procesar una " + "arista con un (2)vertice fuera de rango! i ");
				else {
					nuevaArista(i, j, peso);
				}
				setVisible(false);

			}
		});
		btnAgregar.setBounds(50, 99, 89, 23);
		getContentPane().add(btnAgregar);
	}

	protected void nuevaArista(int i, int j, double peso) {
		// TODO Auto-generated method stub
		LosGrafos.agregarAristas(i, j, peso);
		dibujar.linea(Color.black);
		setVisible(false);

	}

	private void distancia() {
		JLabel lblDistancia = new JLabel("Distancia");
		lblDistancia.setHorizontalAlignment(SwingConstants.LEFT);
		lblDistancia.setBounds(10, 55, 80, 14);
		getContentPane().add(lblDistancia);

		txtDistancia = new JTextField();
		txtDistancia.setText("0");
		txtDistancia.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {// Solo numeros
				aplicar.validarDigitos(e);

			}
		});
		txtDistancia.setBounds(100, 52, 80, 20);
		getContentPane().add(txtDistancia);
	}

	private void vertice2() {
		JLabel lblVertice2 = new JLabel("Vertice 2:");
		lblVertice2.setHorizontalAlignment(SwingConstants.LEFT);
		lblVertice2.setBounds(10, 33, 80, 14);
		getContentPane().add(lblVertice2);

		txtVertice2 = new JTextField();
		txtVertice2.setText(null);
		txtVertice2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				aplicar.validarDigitos(e);
			}
		});
		txtVertice2.setBounds(100, 30, 80, 20);
		getContentPane().add(txtVertice2);

	}

	private void vertice1() {
		JLabel lblVertice1 = new JLabel("Vertice 1:");
		lblVertice1.setHorizontalAlignment(SwingConstants.LEFT);
		lblVertice1.setBounds(10, 11, 80, 14);
		getContentPane().add(lblVertice1);

		txtVertice1 = new JTextField();
		txtVertice1.setText(null);
		txtVertice1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				aplicar.validarDigitos(e);
			}
		});
		txtVertice1.setBounds(100, 8, 80, 20);
		getContentPane().add(txtVertice1);
	}

	private void configuracionInicial() {
		setTitle("::: Nueva Arista :::");
		setResizable(false);
		setBounds(100, 100, 200, 250);
		setSize(200, 157);
		setLocation(100, 0);
		getContentPane().setLayout(null);
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					agregarArista frame = new agregarArista();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
