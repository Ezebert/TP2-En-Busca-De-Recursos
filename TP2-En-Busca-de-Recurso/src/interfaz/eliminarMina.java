package interfaz;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;

import negocio.aplicar;
import negocio.dibujar;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;

import java.awt.event.ActionEvent;


import javax.swing.SwingConstants;

import acceso.LasMinas;
import acceso.LosGrafos;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class eliminarMina extends JFrame {
	private static final long serialVersionUID = 1L;
	private JTextField txtNombre;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					eliminarMina frame = new eliminarMina();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @param x
	 * @param y
	 */
	public eliminarMina() {
		configuracionInicial(200, 170);
		numero();
		btnEliminar();

	}

	private void btnEliminar() {
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(txtNombre.getText().equals("")) {
					JOptionPane.showMessageDialog(null,"Debe ingresar un Nro");
				}
				else{
					int nro = Integer.parseInt(txtNombre.getText()); 
					eliminar(nro);					
				}
				
				setVisible(false);
				
			}
		});
		btnEliminar.setBounds(50, 99, 89, 23);
		getContentPane().add(btnEliminar);
	}

	protected void eliminar(int nro) {
		// TODO Auto-generated method stub
		LasMinas.eliminarMina(nro);
		dibujar.repain(LosGrafos.getGrafo(),Color.yellow, Color.black);
		setVisible(false);

	}

	private void numero() {
		JLabel lblNombre = new JLabel("Nro\r\n");
		lblNombre.setHorizontalAlignment(SwingConstants.LEFT);
		lblNombre.setBounds(10, 11, 80, 14);
		getContentPane().add(lblNombre);

		txtNombre = new JTextField();
		txtNombre.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {//Solo numeros
				aplicar.validarDigitos(e);

			}
		});
		txtNombre.setHorizontalAlignment(SwingConstants.LEFT);
		txtNombre.setBounds(100, 8, 80, 20);
		getContentPane().add(txtNombre);
	}

	private void configuracionInicial(int i, int j) {
		setTitle("::: Eliminar Mina :::");
		setResizable(false);
		setBounds(100, 100, 200, 250);
		setSize(200, 157);
		setLocation(100,0); 
		getContentPane().setLayout(null);

	}
}
