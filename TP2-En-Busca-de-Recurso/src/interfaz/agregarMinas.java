package interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;

import negocio.aplicar;
import negocio.dibujar;

import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.SwingConstants;

import acceso.LasMinas;
import datos.Mina;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Color;

public class agregarMinas extends JFrame {
	private static final long serialVersionUID = 1L;
	private JTextField txtCantCarbon;
	private JTextField txtCoorX;
	private JTextField txtNombre;
	private JTextField txtCoorY;
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					agregarMinas frame = new agregarMinas(800, 600);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param x 
	 * @param y 
	 */
	public agregarMinas(int x, int y) {
		configuracionInicial(200,180);
		numero();
		coorX(x);
		coorY(y);
		cantCarbon();
		btnAgregar();
		
	
		
				

	}

	private void btnAgregar() {
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] nuevaMina = {txtCoorX.getText(),txtCoorY.getText(),txtCantCarbon.getText()};
				agregar(nuevaMina);
			}
		});
		btnAgregar.setBounds(50, 99, 89, 23);
		getContentPane().add(btnAgregar);
	}

	protected void agregar(String[] nuevaMina) {
		// TODO Auto-generated method stub
		Mina nueva = new Mina(Integer.parseInt(nuevaMina[0]),Integer.parseInt(nuevaMina[1]),Integer.parseInt(nuevaMina[2]));
		LasMinas.agregarMina(nueva);
		dibujar.circulo(Color.yellow);
		setVisible(false);
		
	}

	private void cantCarbon() {
		JLabel lblCantCarbon = new JLabel("Cant. Carbon:");
		lblCantCarbon.setHorizontalAlignment(SwingConstants.LEFT);
		lblCantCarbon.setBounds(10, 77, 80, 14);
		getContentPane().add(lblCantCarbon);
		
		txtCantCarbon = new JTextField();
		txtCantCarbon.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {//Solo numeros
				aplicar.validarDigitos(e);

			}
		});
		txtCantCarbon.setText("0");
		txtCantCarbon.setHorizontalAlignment(SwingConstants.LEFT);
		txtCantCarbon.setBounds(100, 74, 80, 20);
		getContentPane().add(txtCantCarbon);
	
	}

	private void coorY(int y) {
		JLabel lblCoorY = new JLabel("Coor Y:");
		lblCoorY.setHorizontalAlignment(SwingConstants.LEFT);
		lblCoorY.setBounds(10, 55, 80, 14);
		getContentPane().add(lblCoorY);
		
		txtCoorY = new JTextField();
		String nombre = Integer.toString(y);
		txtCoorY.setText(nombre);
		txtCoorY.setEditable(false);
		txtCoorY.setHorizontalAlignment(SwingConstants.LEFT);
		txtCoorY.setBounds(100, 52, 80, 20);
		getContentPane().add(txtCoorY);
	}

	private void coorX(int x) {
		JLabel lblCoorX = new JLabel("Coor X:");
		lblCoorX.setHorizontalAlignment(SwingConstants.LEFT);
		lblCoorX.setBounds(10, 33, 80, 14);
		getContentPane().add(lblCoorX);
		
		txtCoorX = new JTextField();
		String nombre = Integer.toString(x);
		txtCoorX.setText(nombre);
		txtCoorX.setEditable(false);
		txtCoorX.setHorizontalAlignment(SwingConstants.LEFT);
		txtCoorX.setBounds(100, 30, 80, 20);
		getContentPane().add(txtCoorX);
	}

	private void numero() {
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setHorizontalAlignment(SwingConstants.LEFT);
		lblNombre.setBounds(10, 11, 80, 14);
		getContentPane().add(lblNombre);
		
		txtNombre = new JTextField();
		txtNombre.setEditable(false);
		txtNombre.setText(Integer.toString(LasMinas.getNro()));
		txtNombre.setHorizontalAlignment(SwingConstants.LEFT);
		txtNombre.setBounds(100, 8, 80, 20);
		getContentPane().add(txtNombre);
	}

	private void configuracionInicial(int i, int j) {
		setTitle("::: Agregar Mina :::");
		setResizable(false);
		setBounds(100, 100, 200, 250);
		setSize(200,167);
		getContentPane().setLayout(null);
		setLocation(100,0); 
		

	
	}
}
