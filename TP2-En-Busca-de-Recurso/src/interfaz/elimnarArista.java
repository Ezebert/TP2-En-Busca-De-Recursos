package interfaz;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;

import negocio.aplicar;
import negocio.dibujar;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import acceso.LasMinas;
import acceso.LosGrafos;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class elimnarArista extends JFrame {
	private static final long serialVersionUID = 1L;
	private JTextField txtVertice1;
	private JTextField txtVertice2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					elimnarArista frame = new elimnarArista();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public elimnarArista() {
		configuracionInicial();

	}

	private void configuracionInicial() {
		// TODO Auto-generated method stub
		setTitle("::: Elimnar Arista :::");
		setResizable(false);
		setBounds(100, 100, 200, 250);
		setSize(200, 157);
		setLocation(100, 0);
		getContentPane().setLayout(null);

		JLabel lblVertice1 = new JLabel("Vertice 1:");
		lblVertice1.setHorizontalAlignment(SwingConstants.LEFT);
		lblVertice1.setBounds(10, 14, 80, 14);
		getContentPane().add(lblVertice1);

		txtVertice1 = new JTextField();
		txtVertice1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				aplicar.validarDigitos(arg0);
			}
		});
		txtVertice1.setText(null);
		txtVertice1.setBounds(100, 11, 80, 20);
		getContentPane().add(txtVertice1);

		JLabel lblVertice2 = new JLabel("Vertice 2:");
		lblVertice2.setHorizontalAlignment(SwingConstants.LEFT);
		lblVertice2.setBounds(10, 36, 80, 14);
		getContentPane().add(lblVertice2);

		txtVertice2 = new JTextField();
		txtVertice2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				aplicar.validarDigitos(e);
			}
		});
		txtVertice2.setText(null);
		txtVertice2.setBounds(100, 33, 80, 20);
		getContentPane().add(txtVertice2);

		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = LasMinas.buscarPosicion(Integer.parseInt(txtVertice1.getText()));
				int j = LasMinas.buscarPosicion(Integer.parseInt(txtVertice2.getText()));
				// Ambos vertice ingresado por el usuario son identicos
				if (i == j)
					JOptionPane.showMessageDialog(null, "Deben ser diferentes (" + i + " " + j + ")");
				if (i < 0 || i >= LosGrafos.getGrafo().numVertices())
					JOptionPane.showMessageDialog(null,
							"Se intento procesar una " + "arista con un (1)vertice fuera de rango! i ");
				if (j < 0 || j >= LosGrafos.getGrafo().numVertices())
					JOptionPane.showMessageDialog(null,
							"Se intento procesar una " + "arista con un (2)vertice fuera de rango! i ");
				else {
					eliminarArista(i, j);
					dibujar.repain(LosGrafos.getGrafo(), Color.yellow, Color.BLACK);

				}
				setVisible(false);
			}
		});
		btnEliminar.setBounds(51, 95, 89, 23);
		getContentPane().add(btnEliminar);

	}

	protected void eliminarArista(int i, int j) {
		// TODO Auto-generated method stub
		LosGrafos.eliminarArista(i, j);
	}
}
