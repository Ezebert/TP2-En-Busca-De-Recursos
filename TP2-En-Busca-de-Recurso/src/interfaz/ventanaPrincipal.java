package interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.border.MatteBorder;

import acceso.LasMinas;
import acceso.LosGrafos;
import datos.GrafoPesado;

import java.awt.Color;
import javax.swing.JButton;

import negocio.algoritmo;
import negocio.aplicar;
import negocio.dibujar;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

public class ventanaPrincipal extends JFrame {
	private static final long serialVersionUID = 1L;
	public static JPanel panel = new JPanel();
	public static JPanel panelDistancia = new JPanel();
	private JTextArea txtArea;
	private static JTextField txtDistancia;
	private static JTextField txtRecurso;

	public ventanaPrincipal() {
		configurarInicial();
		configurarPanel();
		menu();
		btnInforme();
		btnRePain();
		panelDistancia();
		txtArea();
	}

	private void menu() {
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenuItem mntmSalir = new JMenuItem("Salir");
		mntmSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		JMenu mnNewMenu_1 = new JMenu("Archivo");
		menuBar.add(mnNewMenu_1);

		JMenuItem mntmAgregararista = new JMenuItem("AgregarArista");
		mntmAgregararista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				agregarArista();
			}
		});

		JMenu mnAlgoritmos = new JMenu("Algoritmo");
		mnNewMenu_1.add(mnAlgoritmos);

		JMenuItem mntmDijkstra = new JMenuItem("Dijkstra");
		mntmDijkstra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dijkstra();
			}
		});
		mnAlgoritmos.add(mntmDijkstra);

		JMenuItem mntmPrim = new JMenuItem("Prim");
		mntmPrim.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// No se tiene en cuenta los vertices aislados
				primm();
			}
		});
		mnAlgoritmos.add(mntmPrim);
		mnNewMenu_1.add(mntmAgregararista);
		{
			JMenu mnNewMenu = new JMenu("Eliminar");
			menuBar.add(mnNewMenu);

			JMenuItem mntmEliminarMina = new JMenuItem("Mina");
			mntmEliminarMina.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					eliminarMina();

				}
			});
			mnNewMenu.add(mntmEliminarMina);

			JMenuItem mntmArista = new JMenuItem("Arista");
			mntmArista.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					eliminarArista();
				}
			});
			mnNewMenu.add(mntmArista);

			JMenuItem mntmAristatodas = new JMenuItem("Arista (todas)");
			mntmAristatodas.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					eliminarTodasArista();

				}
			});
			mnNewMenu.add(mntmAristatodas);
		}

		JMenu mnHerramientas = new JMenu("Herramientas");
		menuBar.add(mnHerramientas);

		JMenuItem mntmCargarMuestra = new JMenuItem("Cargar Muestras");
		mntmCargarMuestra.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, 0));
		mntmCargarMuestra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				aplicar.cargarMuestra();
				dibujar.repain(LosGrafos.getGrafo(), Color.yellow, Color.black);
			}
		});
		mnHerramientas.add(mntmCargarMuestra);
		mntmSalir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
		menuBar.add(mntmSalir);
	}

	// Aristas
	protected void eliminarArista() {
		// TODO Auto-generated method stub1
		new elimnarArista().setVisible(true);

	}

	protected void agregarArista() {
		// TODO Auto-generated method stub
		new agregarArista().setVisible(true);
	}

	private void eliminarTodasArista() {
		LosGrafos.setGrafo(new GrafoPesado(LasMinas.tamanio()));
		dibujar.repain(LosGrafos.getGrafo(), Color.yellow, Color.black);
	}

	// Minas
	protected void eliminarMina() {
		// TODO Auto-generated method stub
		JOptionPane.showConfirmDialog(null, "En construccion");
		// new eliminarMina().setVisible(true);
	}

	protected void agregarMina(int x, int y) {
		// TODO Auto-generated method stub
		new agregarMinas(x, y).setVisible(true);
	}

	// Algoritmos
	private void primm() {
		System.out.println(LasMinas.tamanio());
		if (algoritmo.esConexo()) {
			GrafoPesado AGM = algoritmo.prim(LosGrafos.getGrafo());
			dibujar.repain(AGM, Color.yellow, Color.red);
			ventanaPrincipal.editarPanelDistancia(AGM.totalPeso());
		} else
			JOptionPane.showMessageDialog(null, "Se se conectan todas las minas por una ruta");

	}

	public static void editarPanelDistancia(String string) {
		// TODO Auto-generated method stub
		panelDistancia.setVisible(true);
		txtDistancia.setText(string + " mts");
		txtRecurso.setText(LasMinas.totalRecurso() + " Carbon");
	}

	protected void dijkstra() {
		// TODO Auto-generated method stub
		new dijkstraFrame().setVisible(true);

	}

	// Btns
	private void btnRePain() {
		JButton btnRePain = new JButton("RePain");
		btnRePain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dibujar.repain(LosGrafos.getGrafo(), Color.yellow, Color.black);
				editarPanelDistancia(LosGrafos.getGrafo().totalPeso());
			}
		});
		btnRePain.setBounds(7, 430, 89, 23);
		getContentPane().add(btnRePain);
	}

	private void btnInforme() {
		JButton btnInforme = new JButton("Informe");
		btnInforme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtArea.setText(null);
				txtArea.setText("Cantidad Minas: " + LasMinas.tamanio());
				txtArea.setText(txtArea.getText() + "\tCantidad de Carbon: "+LasMinas.totalRecurso());
				txtArea.setText(txtArea.getText() + "\nCantidad de Vertice: " + LosGrafos.getGrafo().numVertices());
				txtArea.setText(txtArea.getText() + "\nCantidad de Arista: " + LosGrafos.getGrafo().numAristas());
			
				dibujar.repain(LosGrafos.getGrafo(), Color.yellow, Color.black);
				;
				panelDistancia.setVisible(false);
				System.err.println("---------------------------------------------------------------------");
				System.out.println("ventanaPrincipal.btnInforme().new ActionListener() {...}.actionPerformed()");
				System.out.println(LosGrafos.getGrafo());
				System.err.println("---------------------------------------------------------------------");
			}
		});
		btnInforme.setBounds(7, 468, 80, 26);
		getContentPane().add(btnInforme);
	}

	// OTROS
	private void panelDistancia() {
		panelDistancia.setBounds(7, 24, 80, 205);
		getContentPane().add(panelDistancia);
		panelDistancia.setLayout(null);

		JLabel lblAcumulado = new JLabel("Distancia");
		lblAcumulado.setHorizontalAlignment(SwingConstants.CENTER);
		lblAcumulado.setBounds(0, 12, 80, 42);
		panelDistancia.add(lblAcumulado);

		txtDistancia = new JTextField();
		txtDistancia.setEditable(false);
		txtDistancia.setBounds(0, 70, 80, 20);
		panelDistancia.add(txtDistancia);
		txtDistancia.setColumns(10);

		JLabel lblRecursos = new JLabel("Recursos");
		lblRecursos.setHorizontalAlignment(SwingConstants.CENTER);
		lblRecursos.setBounds(0, 115, 80, 42);
		panelDistancia.add(lblRecursos);

		txtRecurso = new JTextField();
		txtRecurso.setEditable(false);
		txtRecurso.setColumns(10);
		txtRecurso.setBounds(0, 173, 80, 20);
		panelDistancia.add(txtRecurso);
	}

	private void configurarPanel() {
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent event) { // click en el panel
				if (event.getClickCount() == 2) {
					agregarMina(event.getX(), event.getY());
				}
			}
		});
	}

	private void configurarInicial() {
		setResizable(false);
		setTitle("TP FINAL ::: En busca de Recursos :::");
		setBounds(100, 100, 450, 300);
		setSize(800, 600);
		this.setLocation(100, 110);
		getContentPane().setLayout(null);
		panelDistancia.setVisible(false);
	}

	private void txtArea() {
		txtArea = new JTextArea();
		txtArea.setEditable(false);
		txtArea.setBounds(102, 482, 680, 67);
		getContentPane().add(txtArea);
		txtArea.setColumns(10);
		// Configuración del Panel principal
		panel.setBorder(new MatteBorder(1, 1, 3, 3, (Color) Color.BLACK));
		panel.setBounds(102, 22, 680, 442);
		panel.setBackground(Color.LIGHT_GRAY);
		getContentPane().add(panel);
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ventanaPrincipal frame = new ventanaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
