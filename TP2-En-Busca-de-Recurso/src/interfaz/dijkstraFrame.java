package interfaz;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;

import negocio.algoritmo;
import negocio.aplicar;
import negocio.dibujar;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import acceso.LasMinas;
import acceso.LosGrafos;
import datos.GrafoPesado;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JComboBox;

public class dijkstraFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private JTextField txtVertice1;
	private JTextField txtVertice2;
	JComboBox<String> comboBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					aplicar.cargarMuestra();
					dijkstraFrame frame = new dijkstraFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public dijkstraFrame() {
		configuracionInicial();
		vertice1();
		vertice2();
		btnDijkstra();
	}

	private void configuracionInicial() {
		// TODO Auto-generated method stub
		setTitle("::: Dijkstra :::");
		setResizable(false);
		setBounds(100, 100, 200, 250);
		setSize(200, 157);
		setLocation(100, 0);
		getContentPane().setLayout(null);
	}

	private void btnDijkstra() {
		JButton btnEliminar = new JButton("Dijkstra");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = LasMinas.buscarPosicion(Integer.parseInt(txtVertice1.getText()));

				if (comboBox.getSelectedItem().toString().equals("Vertice 2")) {
					int j = LasMinas.buscarPosicion(Integer.parseInt(txtVertice2.getText()));
					calcularDistancia(i, j);
				} else {
					int carbon = Integer.parseInt(txtVertice2.getText());
					extraerRecursos(i, carbon);
				}
				setVisible(false);
			}
		});
		btnEliminar.setBounds(51, 95, 89, 23);
		getContentPane().add(btnEliminar);

	}

	protected void calcularDistancia(int origen, int destino) {
		// TODO Auto-generated method stub
		if (!chequearVertices(origen, destino) && algoritmo.alcanzables(origen).contains(destino)) { // dijkstra
			Integer distancia = algoritmo.calcularDistancia(LosGrafos.getGrafo(), origen, destino);
			if (distancia != Integer.MAX_VALUE)
				JOptionPane.showMessageDialog(null,
						"La distancia Calculada es: " + distancia);
		} else
			JOptionPane.showMessageDialog(null, "No es alcansable la mina seleccionada");

	}

	protected boolean chequearVertices(int i, int j) {
		// TODO Auto-generated method stub
		boolean ret = false;
		if (i == j)
			JOptionPane.showMessageDialog(null, "Deben ser diferentes (" + i + " " + j + ")");
		else if (i < 0 || i >= LosGrafos.getGrafo().numVertices())
			JOptionPane.showMessageDialog(null,
					"Se intento procesar una " + "arista con un (1)vertice fuera de rango! i ");
		else if (j < 0 || j >= LosGrafos.getGrafo().numVertices()) {
			JOptionPane.showMessageDialog(null,
					"Se intento procesar una " + "arista con un (2)vertice fuera de rango! i ");
			ret = true;
		}
		return ret;
	}

	private void vertice2() {

		txtVertice2 = new JTextField();
		txtVertice2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				aplicar.validarDigitos(e);
			}
		});
		txtVertice2.setText("2");
		txtVertice2.setBounds(100, 33, 80, 20);
		getContentPane().add(txtVertice2);

		comboBox = new JComboBox<String>();
		comboBox.addItem("carbon");
		comboBox.addItem("Vertice 2");		
		comboBox.setBounds(7, 33, 80, 20);
		getContentPane().add(comboBox);
	}

	private void vertice1() {
		JLabel lblVertice1 = new JLabel("Vertice 1:");
		lblVertice1.setHorizontalAlignment(SwingConstants.LEFT);
		lblVertice1.setBounds(10, 14, 80, 14);
		getContentPane().add(lblVertice1);

		txtVertice1 = new JTextField();
		txtVertice1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				aplicar.validarDigitos(arg0);
			}
		});
		txtVertice1.setText("1");
		txtVertice1.setBounds(100, 11, 80, 20);
		getContentPane().add(txtVertice1);
	}

	protected void extraerRecursos(int origen, int carbon) {
		// TODO Auto-generated method stub
		GrafoPesado ret = algoritmo.CaminoMinimo(origen,carbon);
		dibujar.repain(ret, Color.yellow,Color.RED);
		ventanaPrincipal.editarPanelDistancia(ret.totalPeso());

	}
}
