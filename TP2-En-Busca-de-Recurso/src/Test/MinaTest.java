package Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import datos.Mina;

public class MinaTest {
	Mina mina,mina3,mina2,mina4;
	@Before
	public void iniciar(){
		mina = new Mina(0, 2);
		mina2 = new Mina(0, 1,100);
		mina3 = new Mina(1, 2);
		mina4 = new Mina(2, 2,50);
	}
	@Test
	public void testGetNro() {
		assertEquals(0,mina.getNro());
		assertEquals(0,mina2.getNro());
	}
	@Test
	public void testGetCantCarbon() {
		assertEquals(50,mina4.getCantCarbon());
		assertEquals(0,mina.getCantCarbon());
	}

	@Test
	public void testGetCoorX_Y() {
		assertEquals(0,mina.getCoorX());
		assertEquals(2,mina.getCoorY());
	}

	@Test
	public void testSet() {
		mina.setCantCarbon(99);
		mina.setCoorX(1);
		mina.setCoorY(2);
		mina.setNro(3);
		assertEquals(99, mina.getCantCarbon());
		assertEquals(1,mina.getCoorX());
		assertEquals(2, mina.getCoorY());
		assertEquals(3,mina.getNro());
		
	}



}
