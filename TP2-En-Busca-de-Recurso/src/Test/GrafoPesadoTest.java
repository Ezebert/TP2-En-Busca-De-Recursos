package Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import datos.GrafoPesado;

public class GrafoPesadoTest {
	GrafoPesado grafo;

	@Before
	public void iniciar() {
		grafo = new GrafoPesado(5);
		grafo.agregarArista(0, 1, 13);
		grafo.agregarArista(1, 2, 12);
		grafo.agregarArista(3, 0, 10);
		grafo.agregarArista(3, 2, 11);
	}

	@Test
	public void agregarArista() {
		grafo.agregarArista(2, 1, 100);
		assertTrue(grafo.existeArista(2, 1));
		assertEquals(5, grafo.numAristas());
		assertEquals(100, grafo.getPeso(2, 1), 1e-4);
	}

	@Test
	public void eliminarArista() {
		grafo.eliminarArista(0, 1);
		assertFalse(grafo.existeArista(0, 1));
		assertEquals(3, grafo.numAristas());
	}

	@Test
	public void pesosTest() {
		assertEquals(13, grafo.getPeso(0, 1), 1e-4);
		assertEquals(10, grafo.getPeso(3, 0), 1e-4);
		assertEquals(11, grafo.getPeso(3, 2), 1e-4);
		assertEquals(12, grafo.getPeso(1, 2), 1e-4);
	}

	@Test(expected = IllegalArgumentException.class)
	public void pesoFallidoTest() {
		grafo.getPeso(0, 2);
	}

	@Test
	public void contieneAristaTest() {
		assertTrue(grafo.existeArista(0, 1));
		assertTrue(grafo.existeArista(3, 0));
		assertTrue(grafo.existeArista(3, 2));
		assertTrue(grafo.existeArista(1, 2));
	}

	@Test
	public void noContineArista() {
		assertFalse(grafo.existeArista(1, 0));
	}

	@Test
	public void totalPeso() {
		assertEquals("46.0", grafo.totalPeso());
	}

}
