package Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import acceso.LasMinas;
import acceso.LosGrafos;
import datos.GrafoPesado;
import datos.Mina;
import negocio.aplicar;

public class LosGrafosTest {
	@Before
	public void iniciar() {
		aplicar.cargarMinasMuestra();
		GrafoPesado grafo = new GrafoPesado(6);
		grafo.agregarArista(0, 1, 13);
		grafo.agregarArista(1, 2, 12);
		grafo.agregarArista(3, 0, 10);
		grafo.agregarArista(3, 2, 11);//46
		LosGrafos.setGrafo(grafo);
	}

	@Test
	public void testAgregarAristas() {
		LosGrafos.agregarAristas(2, 3, 50);
		assertEquals(5, LosGrafos.getGrafo().numAristas());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAgregarAristasIgualesVertices() {
		LosGrafos.agregarAristas(0, 0, 5);
	}

	@Test
	public void testEliminarArista() {
		LosGrafos.eliminarArista(0, 1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testEliminarAristaNoExistente() {
		LosGrafos.eliminarArista(3, 1);
	}

	@Test
	public void testActualizarVertices() { 
		LasMinas.agregarMina(new Mina(1, 1));
		assertEquals(7, LosGrafos.getGrafo().numVertices());
	}

	@Ignore
	public void testEliminarAristaVinculadas() {
		LosGrafos.eliminarAristaVinculadas(0);
		assertEquals(2,LosGrafos.getGrafo().numAristas());
	}

	@Ignore
	public void testActualizarVerticesInt() {
		fail("Not yet implemented");
	}

	@Test
	public void testTotalPeso() {
		assertEquals(46, LosGrafos.totalPeso(), 1e-4);
	}

}
