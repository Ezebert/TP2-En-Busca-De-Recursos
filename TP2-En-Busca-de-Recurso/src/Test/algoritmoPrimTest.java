package Test;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import acceso.LasMinas;
import datos.GrafoPesado;
import negocio.algoritmo;
import negocio.aplicar;

public class algoritmoPrimTest {
	GrafoPesado grafo;

	@Before
	public void iniciar() {
		grafo = new GrafoPesado(6);
		grafo.agregarArista(0, 1, 3);
		grafo.agregarArista(0, 3, 2);
		grafo.agregarArista(0, 5, 0);
		grafo.agregarArista(1, 2, 4);
		grafo.agregarArista(1, 3, 1);
		grafo.agregarArista(2, 4, 4);
		grafo.agregarArista(3, 4, 2);
		grafo.agregarArista(4, 0, 8);
	}

	@Test
	public void testPrim() {
		GrafoPesado agm = algoritmo.prim(grafo);
		assertTrue(agm.existeArista(0, 1));
		assertTrue(agm.existeArista(0, 3));
		assertTrue(agm.existeArista(1, 2));
		assertTrue(agm.existeArista(3, 4));

		assertEquals(5, agm.numAristas());
	}

	@Test
	public void testMenorArista() {
		Set<Integer> visitados = new HashSet<>();
		visitados.add(0);
		algoritmo.Arista arista = algoritmo.menorArista(grafo, visitados);
		assertEquals(new algoritmo.Arista(0, 5, 0), arista);
	}

	@Test
	public void testMenorAristaTres() {
		Set<Integer> visitados = new HashSet<>();
		visitados.add(3);
		visitados.add(1);
		visitados.add(2);

		algoritmo.Arista arista = algoritmo.menorArista(grafo, visitados);
		assertEquals(new algoritmo.Arista(3, 4, 2), arista);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEsConexo() {
		assertEquals(grafo.numVertices(), algoritmo.alcanzables(0).size());
	}
	
	@Test
	public void testEsConexoCargarMinas() {
		aplicar.cargarMuestra();
		assertEquals(grafo.numVertices(), algoritmo.alcanzables(0).size());
	}
	
	@Test
	public void testAlcanzablesTodos() {
		aplicar.cargarMuestra();
		Set<Integer> alcanzables = algoritmo.alcanzables(0);
		assertIguales(new int[] {0, 1, 2, 3,4,5}, alcanzables);
	}
	
	private void assertIguales(int[] is, Set<Integer> alcanzables) {
		// TODO Auto-generated method stub
		assertEquals(is.length, alcanzables.size());
		
		for(Integer valor: is)
			assertTrue(alcanzables.contains(valor));
	}

	@After
	public void finalizar(){
		LasMinas.getlistMina().removeAll(LasMinas.getlistMina());
	}
	

}
