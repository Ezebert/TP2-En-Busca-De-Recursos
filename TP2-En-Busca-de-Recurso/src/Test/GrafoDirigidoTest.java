/**
 * 
 */
package Test;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import datos.GrafoDirigido;

/**
 * @author Ezebert
 *
 */
public class GrafoDirigidoTest {
	GrafoDirigido grafo;

	@Before
	public void iniciar() {
		grafo = instancia();
	}
	
	private GrafoDirigido instancia() {
		GrafoDirigido grafo = new GrafoDirigido(5);
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(1, 3);
		grafo.agregarArista(2, 3);
		grafo.agregarArista(3, 4);
		grafo.agregarArista(2, 4);

		return grafo;
	}

	@Test
	public void testAgregarArista() {
		assertTrue(grafo.existeArista(0, 1));
		assertTrue(grafo.existeArista(0, 2));
		assertTrue(grafo.existeArista(1, 3));
		assertFalse(grafo.existeArista(1, 0));
		assertFalse(grafo.existeArista(2, 0));
		assertFalse(grafo.existeArista(3, 1));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarAristaIlegal() {
		grafo.agregarArista(-1, 2);
		grafo.agregarArista(-1, 0);
		grafo.agregarArista(-8, 8);
		grafo.agregarArista(1, 1);
		assertEquals(6, grafo.numAristas());
	}
	
	@Test
	public void testBorrarArista() {

		grafo.borrarArista(0, 1);
		assertEquals(5, grafo.numAristas());
		assertFalse(grafo.existeArista(0, 1));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testBorrarAristaRepetida() {
		grafo.borrarArista(0, 1);
		grafo.borrarArista(0, 1);
		assertEquals(5, grafo.numAristas());
		assertFalse(grafo.existeArista(0, 1));
	}
	
	@Test
	public void testNumVertices() {
		assertEquals(grafo.numVertices(), 5);
	}

	@Test
	public void testNumAristas() {
		assertEquals(grafo.numAristas(), 6);
	}

	@Test
	public void testVecinos() {
		grafo.agregarArista(0, 4);
		Set<Integer> vecinos = grafo.vecinos(0);
		assertEquals(3, vecinos.size());
		assertTrue(vecinos.contains(1));
		assertTrue(vecinos.contains(2));
		assertTrue(vecinos.contains(4));
		grafo.borrarArista(0, 4);
		assertFalse(vecinos.contains(4));
	}

}
