package Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import acceso.LasMinas;
import datos.Mina;

public class LasMinasTest {

	@Before
	public void iniciar(){
		LasMinas.agregarMina(new Mina (1,1,10));
		LasMinas.agregarMina(new Mina(1, 2));
	}

	@Test
	public void testminaExistente() {
		assertTrue(LasMinas.minaExistente(new Mina(1,2)));
		}
	
	@Test
	public void testNuevaMina() {
		LasMinas.agregarMina(new Mina(2, 2,20));
		assertEquals(3, LasMinas.tamanio()); //Tamanio
		assertTrue(LasMinas.minaExistente(new Mina(2, 2)));
	}

	@Ignore
	public void testEliminarMina() {
		fail("Not yet implemented");
	}

	@Test
	public void testBuscarPosicion() {
		assertEquals(0,LasMinas.buscarPosicion(1)); //Primera posicion
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testBuscarPosicionNoExistente() {
		assertEquals(0,LasMinas.buscarPosicion(44)); //Primera posicion
	}

	@Test
	public void testTotalRecurso() {
		assertEquals("10", LasMinas.totalRecurso());
	}

//	
}

