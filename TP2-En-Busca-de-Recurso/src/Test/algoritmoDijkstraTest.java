package Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import datos.GrafoPesado;
import negocio.algoritmo;
public class algoritmoDijkstraTest {
	private GrafoPesado grafo;

	@Before
	public void iniciar(){
		 grafo = new GrafoPesado(6);
		grafo.agregarArista(0, 1, 3);
		grafo.agregarArista(0, 3, 2);
		grafo.agregarArista(1, 2, 4);
		grafo.agregarArista(1, 3, 1);
		grafo.agregarArista(2, 4, 4);
		grafo.agregarArista(3, 4, 2);		
		grafo.agregarArista(4, 0, 8);
		grafo.agregarArista(0, 5, 4);
	}

	@Test
	public void testDijkstra_extraerRecursos() {
		fail("Not yet implemented");
	}

	@Test
	public void testCalcularDistancia() {
		assertEquals(11, algoritmo.calcularDistancia(grafo, 1, 0));
		assertEquals(0, algoritmo.calcularDistancia(grafo, 1, 1));
		assertEquals(4, algoritmo.calcularDistancia(grafo, 1, 2));
		assertEquals(1, algoritmo.calcularDistancia(grafo, 1, 3));
		assertEquals(3, algoritmo.calcularDistancia(grafo, 1, 4));
		assertEquals(15, algoritmo.calcularDistancia(grafo, 1, 5));
	}

}
