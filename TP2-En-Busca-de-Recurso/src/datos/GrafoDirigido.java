package datos;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class GrafoDirigido {
	// Representamos un grafo por listas de vecinos
	private ArrayList<HashSet<Integer>> _vecinos;

	public GrafoDirigido(int vertices) {
		_vecinos = new ArrayList<HashSet<Integer>>(vertices);
		for (int i = 0; i < vertices; i++)
			_vecinos.add(new HashSet<Integer>());
	}

	public void agregarArista(int i, int j) {
		chequearIndices(i, j);// Extremos
		if (!existeArista(i, j)) {
			_vecinos.get(i).add(j);
		}

	}

	public void borrarArista(int i, int j) {
		chequearIndices(i, j);
		chequearQueExista(i, j);

		_vecinos.get(i).remove(j);

	}

	public boolean existeArista(int i, int j) {
		chequearIndices(i, j);
		return _vecinos.get(i).contains(j);
	}

	public int numVertices() {
		return _vecinos.size();
	}

	public int numAristas() {
		int ret = 0;
		for (int i = 0; i < numVertices(); i++)
			for (int j = 0; j < numVertices(); j++)
				if (i != j && existeArista(i, j))
					++ret;
		return ret;
	}

	private void chequearQueExista(int i, int j) {
		// TODO Auto-generated method stub
		if (!existeArista(i, j))
			throw new IllegalArgumentException("La arista (" + i + ", " + j + ") no existe!");

	}

	private void chequearIndices(int i, int j) {
		// TODO Auto-generated method stub
		if (i < 0 || i >= numVertices())
			throw new IllegalArgumentException(
					"Se intento procesar una " + "arista con un vertice fuera de rango! i = " + i);

		if (j < 0 || j >= numVertices())
			throw new IllegalArgumentException(
					"Se intento procesar una " + "arista con un vertice fuera de rango! j = " + j);

		if (i == j)
			throw new IllegalArgumentException(
					"Se intento procesar una " + "arista entre un vertice y el mismo! i = j = " + i);

	}

	@Override
	public String toString() {
		String grafo = "";
		for (int i = 0; i < this._vecinos.size(); i++) {
			grafo += "V�rtice: " + i + " Vecinos: " + this._vecinos.get(i).toString() + "\n";
		}
		return grafo;
	}

	public Set<Integer> vecinos(int i) {
		chequearVertice(i);
		return _vecinos.get(i); // :)
	}

	// Protesta si el vertice i no existe
	private void chequearVertice(int i) {
		if (i < 0 || i >= numVertices())
			throw new IllegalArgumentException("Se consult� un " + "v�rtice inexistente! i = " + i);
	}

}
