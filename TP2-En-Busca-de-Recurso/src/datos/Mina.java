package datos;

public class Mina {
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cantCarbon;
		result = prime * result + coorX;
		result = prime * result + coorY;
		result = prime * result + nro;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Mina)) {
			return false;
		}
		Mina other = (Mina) obj; 
//		if (cantCarbon != other.cantCarbon) {//si existe la ubicacion se setea
//			return false;
//		}
		if (coorX != other.coorX) {
			return false;
		}
		if (coorY != other.coorY) {
			return false;
		}
//		if (nro != other.nro) { //es una referencia
//			return false;
//		}
		return true;
	}
	private int nro;
	private int cantCarbon;
	private int coorX;
	private int coorY;
	
	public Mina(int coorX, int coorY) {
			this.coorX = coorX;
			this.coorY = coorY;
		}
	

	public Mina(int coorX, int coorY, int cantCarbon) {
		this.cantCarbon = cantCarbon;
		this.coorX = coorX;
		this.coorY = coorY;
	}

	public int getNro() {
		return nro;
	}
	public int getCantCarbon() {
		return cantCarbon;
	}
	public int getCoorX() {
		return coorX;
	}
	public int getCoorY() {
		return coorY;
	}
	public void setNro(int nro) {
		this.nro = nro;
	}
	public void setCantCarbon(int i) {
		this.cantCarbon = i;
	}
	public void setCoorX(int coorX) {
		this.coorX = coorX;
	}
	public void setCoorY(int coorY) {
		this.coorY = coorY;
	}
	@Override
	public String toString() {
		return nro + ": ("+coorX +" : "+coorY+") = "+cantCarbon+"\n";
	}
	
	
		
}
