package datos;

public class GrafoPesado extends GrafoDirigido {
	// Matriz de pesos
	private double _pesos[][];

	public GrafoPesado(int vertices) {
		super(vertices);
		_pesos = new double[vertices][vertices];
	}

	public void agregarArista(int i, int j, double peso) {

		this.agregarArista(i, j);
		this._pesos[i][j] = peso;
	}

	public void eliminarArista(int i, int j) {
		borrarArista(i, j);
	}
	
	//Consultas
	@Override
	public String toString() {
		return super.toString() + " " + _pesos;
	}

	public boolean tieneAristas() {
		return numAristas() > 0;
	}


	public double getPeso(int i, int j) {
		if( existeArista(i, j) == false )
			throw new IllegalArgumentException("Se consulto el peso de una arista inexistente! " + i + ", " + j);
		return _pesos[i][j];	
	}

	public void setPeso(double[][] peso) {
		this._pesos=peso; 
	}

	public double[][] getPesos() {
		return _pesos;
	}

	public String totalPeso(){
		double ret=0;
		for(int i = 0;i< numVertices();i++)
			for(int j = 0;j< numVertices();j++)
				if(i!= j &&existeArista(i, j))
					ret +=getPeso(i, j);
		return Double.toString(ret);
	}
	
}
