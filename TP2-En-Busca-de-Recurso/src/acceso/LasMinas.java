package acceso;


import java.util.ArrayList;

import javax.swing.JOptionPane;

import datos.Mina;

public class LasMinas {
	private static ArrayList<Mina> _listMina = new ArrayList<Mina>();
	private static int nro = 1;

	public static ArrayList<Mina> getlistMina() {
		return _listMina;
	}

	public static void setlistMina(ArrayList<Mina> listMina) {
		_listMina = listMina;
	}

	@Override
	public String toString() {
		return _listMina + "]";
	}

	public static void agregarMina(Mina nueva) {
		// TODO Auto-generated method stub
		if (!minaExistente(nueva)) {
			nueva.setNro(nro);
			nro++;
			getlistMina().add(nueva);
			LosGrafos.actualizarVertices();
		}

	}

	public static boolean minaExistente(Mina nueva) {
		// TODO Auto-generated method s
		return getlistMina().contains(nueva);
		
	}

	public static int getNro() {
		// TODO Auto-generated method stub
		return nro;
	}

	public static int tamanio() {
		return _listMina.size();

	}

	public static void eliminarMina(int nro) {
		// TODO Auto-generated method stub
		if(tamanio()==0) JOptionPane.showMessageDialog(null, "No se ingresaron Minas aun");
		else{ //tenemos minas para buscar
			int pos = buscarPosicion(nro); // -1 no existe la mina
			if(pos<0) JOptionPane.showMessageDialog(null, "No existe la mina ingresa "+nro);
			else{ //Existe la mina
 				LosGrafos.eliminarAristaVinculadas(pos);
				_listMina.remove(pos); //elimino
				LosGrafos.actualizarVertices();
			}
		}
	}

	public static int buscarPosicion(int nro) {
		// TODO Auto-generated method stub
		int ret= -1;
		for (int i = 0; i < tamanio(); i++) {
			if(_listMina.get(i).getNro()==nro)
				ret = i;
		}
		if(ret == -1)
			throw new IllegalArgumentException("Posicion Inexistente");
		return ret;
	}

	public static void setNro(int i) {
		// TODO Auto-generated method stub
		nro = i;
		
	}

	public static String totalRecurso() {
		// TODO Auto-generated method stub
		int ret=0;
		for (Mina mina : _listMina) {
			ret+=mina.getCantCarbon();
		}
		return Integer.toString(ret);
	}
		
		


}
