package acceso;

import datos.GrafoPesado;

public class LosGrafos {
	protected static GrafoPesado _grafo = null;

	public static void setGrafo(GrafoPesado grafo) {
		// TODO Auto-generated method stub
		_grafo = grafo;
	}

	public static GrafoPesado getGrafo() {
		// TODO Auto-generated method stub
		if (LasMinas.tamanio() == 0) {
			throw new IllegalArgumentException("No se ingresaron minas aun");
		}
		if (estaVacia())
			// return new GrafoPesado(A_minas.getMina().size());
			return new GrafoPesado(LasMinas.tamanio());

		// actualizo los vertices
		return _grafo;

	}

	private static boolean estaVacia() {
		// TODO Auto-generated method stub
		return _grafo == null; // ||A_minas.getMina().size() == 0;
	}

	public static void agregarAristas(int i, int j, double peso) {
		// TODO Auto-generated method stub
		_grafo = getGrafo();
		if (!_grafo.existeArista(i, j)) { // No existe , agregar
			_grafo.agregarArista(i, j, peso);
			setGrafo(_grafo);
		} else
			throw new IllegalArgumentException ("Ya Existe la arista (" + i + "-" + j + ")");
	}

	public static void eliminarArista(int i, int j) {
		// TODO Auto-generated method stub

		if (getGrafo().existeArista(i, j)) { // existe , borro
			getGrafo().borrarArista(i, j);
		} else
			throw new IllegalArgumentException("No existe" + i + "-" + j + ")");

	}

	public static void main(String[] args) {

	}

	public static void actualizarVertices() {
		// TODO Auto-generated method stub
		GrafoPesado ret = new GrafoPesado(LasMinas.tamanio());
		ret = copiar(ret);
		setGrafo(ret);
	}

	private static GrafoPesado copiar(GrafoPesado ret) {
		// TODO Auto-generated method stub
		for (int i = 0; i < getGrafo().numVertices(); i++)
			for (int j = 0; j < getGrafo().numVertices(); j++)
				if (i != j && getGrafo().existeArista(i, j)) {
					ret.agregarArista(i, j, getGrafo().getPeso(i, j));
				}
		return ret;
	}

	public static void eliminarAristaVinculadas(int pos) {
		// TODO Auto-generated method stub
		GrafoPesado ret = getGrafo();
		for (int i = 0; i < ret.numVertices(); i++) {
			for (int j = 0; j < ret.numVertices(); j++) {
				if (pos != j && ret.existeArista(pos, j))
					ret.borrarArista(pos, j);
				if (i != pos && ret.existeArista(i, pos))
					ret.borrarArista(i, pos);
			}
		}
		setGrafo(ret);
	}

	public static void actualizarVertices(int pos) {
		// TODO Auto-generated method stub
		GrafoPesado ret = new GrafoPesado(LasMinas.tamanio());
		for (int i = 0; i < getGrafo().numVertices(); i++)
			for (int j = 0; j < getGrafo().numVertices(); j++)
				if (i != j && getGrafo().existeArista(i, j))
					if (pos != i && pos != j)
						// System.err.println(i +" "+j);
						ret.agregarArista(i, j, i);
		setGrafo(ret);

	}

	public static double totalPeso() {
		double ret = 0;
		for (int i = 0; i < getGrafo().numVertices(); i++)
			for (int j = 0; j < getGrafo().numVertices(); j++)
				if(i!=j && getGrafo().existeArista(i, j))
					ret += getGrafo().getPeso(i, j);
		return ret;
	}

}
