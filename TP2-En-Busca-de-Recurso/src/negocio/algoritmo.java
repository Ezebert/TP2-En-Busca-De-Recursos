package negocio;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import acceso.LasMinas;
import acceso.LosGrafos;
import datos.GrafoPesado;

public class algoritmo {
	public static GrafoPesado prim(GrafoPesado grafo) { // Arbol Generador Min
		// TODO Auto-generated method stub
		GrafoPesado ret = new GrafoPesado(grafo.numVertices());
		Set<Integer> visitados = new HashSet<Integer>();
		visitados.add(0);
		for (int i = 0; i < grafo.numVertices() - 1; ++i) {
			Arista a = menorArista(grafo, visitados);
			if (a.peso != Double.MIN_VALUE && a.origen != a.destino) {
				ret.agregarArista(a.origen, a.destino, a.peso);
				visitados.add(a.destino);
			}
		}
		return ret;
	}

	// Disjktras
	public static GrafoPesado CaminoMinimo(int origen, int carbon) {
		GrafoPesado ret = new GrafoPesado(8);
		ArrayList<Integer> distancia = new ArrayList<Integer>();
		ArrayList<Integer> padre = new ArrayList<Integer>();
		ArrayList<Boolean> visto = new ArrayList<Boolean>();
		ArrayList<Boolean> todosVisitados = new ArrayList<Boolean>();
		// inicializacion
		for (int index = 0; index < LosGrafos.getGrafo().numVertices(); index++) {
			distancia.add(Integer.MAX_VALUE);
			padre.add(Integer.MAX_VALUE);
			visto.add(false);
			todosVisitados.add(true);
		}
		double[][] peso = LosGrafos.getGrafo().getPesos();
		distancia.set(origen, 0);
		while (!todosVisitados.containsAll(visto)) { // mientras que no este
														// todo visto
			int vertice = minDistancia(distancia, visto);
			visto.set(vertice, true);
			for (int index = 0; index < LosGrafos.getGrafo().numVertices(); index++) {

				if (!visto.get(index) && peso[vertice][index] != 0 && distancia.get(vertice) != Integer.MAX_VALUE
						&& distancia.get(vertice) + peso[vertice][index] < distancia.get(index)) {
					distancia.set(index, (int) (distancia.get(vertice) + peso[vertice][index]));
					padre.set(index, vertice);
				} // Fin if

			} // Fin For
		} // Fin while
		armarGrafo(ret, padre);

		return extraerRecursos(ret, carbon);
	}// FIn Funcion
	// Extraer caer

	private static GrafoPesado extraerRecursos(GrafoPesado grafo, int carbon) {
		// TODO Auto-generated method stub
		GrafoPesado ret = new GrafoPesado(grafo.numVertices());
		Set<Integer> visitados = new HashSet<Integer>();
		int carbonENcontrado = 0;
		visitados.add(0);
		for (int i = 0; i < grafo.numVertices() - 1; ++i) {
			Arista a = menorArista(grafo, visitados);
			if (a.peso != Double.MIN_VALUE && a.origen != a.destino) {
				System.err.println(a.toString() + " " + LasMinas.getlistMina().get(a.destino).getCantCarbon());
				ret.agregarArista(a.origen, a.destino, a.peso);
				visitados.add(a.destino);
				if (carbonENcontrado < carbon && LasMinas.getlistMina().get(a.destino).getCantCarbon() > 0) { // 0<150
																												// si
					carbonENcontrado += LasMinas.getlistMina().get(a.destino).getCantCarbon();
					LasMinas.getlistMina().get(a.destino).setCantCarbon(0);
					if (carbonENcontrado > carbon) { // finalizar y controlar
														// exceso
						// el carbon , tengo que setear
						LasMinas.getlistMina().get(a.destino).setCantCarbon(carbonENcontrado - carbon); // extraigo
						carbonENcontrado = carbon;
						return ret;
					}
				}

			}
		}
		return ret;

	}

	// armamos un grafo Dijkstra
	private static void armarGrafo(GrafoPesado ret, ArrayList<Integer> padre) {
		int index = 0;
		while (index <= padre.size() - 1) {
			if (padre.get(index) != Integer.MAX_VALUE)// tengo mi arbol
				ret.agregarArista(padre.get(index), index, LosGrafos.getGrafo().getPeso(padre.get(index), index));
			index++;
		}
	}

	// devuelve el index de la menor distancai
	private static int minDistancia(ArrayList<Integer> distancia, ArrayList<Boolean> visto) {
		// TODO Auto-generated method stub
		int min = Integer.MAX_VALUE;
		int min_index = -1;

		for (int index = 0; index < distancia.size(); index++) {
			if (!visto.get(index) && distancia.get(index) <= min) {
				min = distancia.get(index);
				min_index = index;
			}
		}
		return min_index;
	}

	// devuelve la distancia de un origen a un destino buscando el camino mas
	// corto // https://es.wikipedia.org/wiki/Algoritmo_de_Dijkstra

	public static int calcularDistancia(GrafoPesado grafo, int origen, int destino) {

		ArrayList<Integer> distancia = new ArrayList<Integer>();
		ArrayList<Boolean> visto = new ArrayList<Boolean>();
		ArrayList<Boolean> todosVisitados = new ArrayList<Boolean>();
		// inicializacion
		for (int index = 0; index < LosGrafos.getGrafo().numVertices(); index++) {
			distancia.add(Integer.MAX_VALUE);
			visto.add(false);
			todosVisitados.add(true);
		}
		double[][] peso = LosGrafos.getGrafo().getPesos();

		distancia.set(origen, 0);
		while (!todosVisitados.containsAll(visto)) { // mientras que no este
														// todo visto
			int vertice = minDistancia(distancia, visto);

			visto.set(vertice, true);
			for (int index = 0; index < LosGrafos.getGrafo().numVertices(); index++) {
				if (!visto.get(index) && peso[vertice][index] != 0 && distancia.get(vertice) != Integer.MAX_VALUE
						&& distancia.get(vertice) + peso[vertice][index] < distancia.get(index)) {
					distancia.set(index, (int) (distancia.get(vertice) + peso[vertice][index]));
				}

			} // Fin For
		} // Fin while
		return distancia.get(destino);

	}

	// verifica que se unan todos sus vertices
	public static boolean esConexo() {
		return alcanzables(0).size() == LosGrafos.getGrafo().numVertices();
	}

	// Encuentra los vertices que se pueden llegar dado un origen
	public static Set<Integer> alcanzables(int origen) {
		Set<Integer> marcados = new HashSet<Integer>();
		List<Integer> pendientes = new ArrayList<Integer>();
		pendientes.add(origen);

		while (pendientes.size() > 0) {
			int i = pendientes.get(0);
			marcados.add(i);

			for (Integer vecino : LosGrafos.getGrafo().vecinos(i)) {
				if (!marcados.contains(vecino) && !pendientes.contains(vecino))
					pendientes.add(vecino);
			}
			pendientes.remove(pendientes.indexOf(i));
		}
		return marcados;
	}

	// Inner class
	// Class inner
	public static class Arista {
		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "Arista [origen=" + origen + ", destino=" + destino + ", peso=" + peso + "]";
		}

		public Integer origen;
		public Integer destino;
		public double peso;

		public Arista(int origen, int destino, double pesoArista) {
			this.origen = origen;
			this.destino = destino;
			this.peso = pesoArista;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;

			if (obj == null)
				return false;

			if (obj instanceof Arista) {
				Arista otra = (Arista) obj;
				if (Objects.equals(origen, otra.origen) && Objects.equals(destino, otra.destino))
					return true;
			}
			return false;
		}

		@Override
		public int hashCode() {
			return this.origen.hashCode() + this.destino.hashCode();
		}

	}

	// Retorna la arista de menor peso entre un vertice amarillo y uno no
	// amarillo
	// Busca la menor arista
	public static Arista menorArista(GrafoPesado grafo, Set<Integer> visitados) {
		Arista ret = new Arista(0, 0, Double.MAX_VALUE);
		for (Integer i : visitados)
			for (Integer j : grafo.vecinos(i)) {
				if (visitados.contains(j) == false) {
					if (grafo.getPeso(i, j) < ret.peso)
						ret = new Arista(i, j, grafo.getPeso(i, j));
				}
			}

		return ret;
	}

	// Retorna la arista de menor peso entre un vertice amarillo y uno no

}