package negocio;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.ArrayList;

import acceso.LasMinas;
import acceso.LosGrafos;
import datos.GrafoPesado;
import datos.Mina;
import interfaz.ventanaPrincipal;

public class dibujar {

	public static void circulo(Color color) {
		// TODO Auto-generated method stub

		ArrayList<Mina> listMina = LasMinas.getlistMina();
		Graphics2D g = (Graphics2D) ventanaPrincipal.panel.getGraphics();
		if (listMina.size() > 0) {
			for (int i = 0; i < LosGrafos.getGrafo().numVertices(); i++) {
				// for(int i =0;i<listMina.size();i++){
				g.setPaint(color);
				g.setStroke(new BasicStroke(4));// leda el grosor al
				g.fillOval(listMina.get(i).getCoorX(), listMina.get(i).getCoorY(), 15, 15);

				g.setColor(Color.blue);
				Font font = new Font("Monospaced", Font.BOLD, 10);
				g.setFont(font);
				String nombre = listMina.get(i).getNro() + "(" + listMina.get(i).getCantCarbon() + ")";
				g.drawString(nombre, listMina.get(i).getCoorX(), listMina.get(i).getCoorY());
			}
		}

	}

	public static void linea(Color color) {
		ArrayList<Mina> listMina = LasMinas.getlistMina();
		GrafoPesado grafo = LosGrafos.getGrafo();
		Graphics2D g = (Graphics2D) ventanaPrincipal.panel.getGraphics();

		for (int i = 0; i < grafo.numVertices(); i++) {
			for (int j = 0; j < grafo.numVertices(); j++) {
				if (i != j && grafo.existeArista(i, j)) {
					g.setColor(color);
					int x1 = listMina.get(i).getCoorX();
					int y1 = listMina.get(i).getCoorY();
					int x2 = listMina.get(j).getCoorX();
					int y2 = listMina.get(j).getCoorY();
					g.drawLine(x1 + 10, y1 + 10, x2 + 10, y2 + 10);
					int aux_x = 0, aux_y = 0;
					if (x1 <= x2)
						aux_x = ((x2 - x1) / 2) + x1;
					if (x1 > x2)
						aux_x = ((x1 - x2) / 2) + x2;
					if (y1 < y2)
						aux_y = ((y2 - y1) / 2) + y1;
					if (y1 >= y2)
						aux_y = ((y1 - y2) / 2) + y2;
					g.setColor(Color.white);
					Font fuente = new Font("Monospaced", Font.PLAIN, 10);
					g.setFont(fuente);
					g.drawString(String.valueOf(grafo.getPeso(i, j)), aux_x, aux_y);
				}

			}
		}

	}

	public static void repain(GrafoPesado ret, Color yellow, Color black) {
		// TODO Auto-generated method stub
		ventanaPrincipal.panel.paint(ventanaPrincipal.panel.getGraphics());
		circulo(yellow);
		linea(ret, black);

	}

	public static void linea(GrafoPesado grafo, Color color) {
		// TODO Auto-generated method stub
		ArrayList<Mina> listMina = LasMinas.getlistMina();
		Graphics2D g = (Graphics2D) ventanaPrincipal.panel.getGraphics();

		for (int i = 0; i < grafo.numVertices(); i++) {
			for (int j = 0; j < grafo.numVertices(); j++) {
				if (i != j && grafo.existeArista(i, j)) {
					g.setColor(color);
					int x1 = listMina.get(i).getCoorX();
					int y1 = listMina.get(i).getCoorY();
					int x2 = listMina.get(j).getCoorX();
					int y2 = listMina.get(j).getCoorY();
					g.drawLine(x1 + 10, y1 + 10, x2 + 10, y2 + 10);

					int aux_x = 0, aux_y = 0;
					if (x1 <= x2)
						aux_x = ((x2 - x1) / 2) + x1;
					if (x1 > x2)
						aux_x = ((x1 - x2) / 2) + x2;
					if (y1 < y2)
						aux_y = ((y2 - y1) / 2) + y1;
					if (y1 >= y2)
						aux_y = ((y1 - y2) / 2) + y2;

					g.setColor(Color.white);
					Font fuente = new Font("Monospaced", Font.PLAIN, 10);
					g.setFont(fuente);
					g.drawString(String.valueOf(grafo.getPeso(i, j)), aux_x, aux_y);
				}

			}
		}
	}

}
