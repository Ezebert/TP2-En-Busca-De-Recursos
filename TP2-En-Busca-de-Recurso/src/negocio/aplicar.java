package negocio;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import acceso.LasMinas;
import acceso.LosGrafos;
import datos.GrafoPesado;
import datos.Mina;

public class aplicar {

	public static void cargarMuestra() {
		// TODO Auto-generated method stub
		cargarMinasMuestra();
		cargarGrafoPrueba();
	}

	public static void cargarGrafoPrueba() {
		GrafoPesado grafo = new GrafoPesado(LasMinas.tamanio());
		grafo.agregarArista(0, 1, 4);
		grafo.agregarArista(1, 2, 3);
		grafo.agregarArista(2, 3, 1);
		grafo.agregarArista(4, 3, 4);
		grafo.agregarArista(5, 4, 3);
		grafo.agregarArista(0, 5, 3);		
		grafo.agregarArista(0, 2, 8);
		grafo.agregarArista(1, 4, 1);
		grafo.agregarArista(2, 4, 1);
		LosGrafos.setGrafo(grafo);
	}

	public static void cargarMinasMuestra() {
		ArrayList<Mina> listMina =new ArrayList<Mina>();
		listMina.add(new Mina(160, 270));
		listMina.add(new Mina(230, 130));
		listMina.add(new Mina(370, 120));
		listMina.add(new Mina(460, 230,100));
		listMina.add(new Mina(350, 360, 100));
		listMina.add(new Mina(200, 360,100));
		LasMinas.setlistMina(listMina);
		for(int i=0;i<listMina.size();i++)
			listMina.get(i).setNro(i);
		LasMinas.setlistMina(listMina);
		LasMinas.setNro(LasMinas.tamanio());
	}

	public static void validarDigitos(KeyEvent e) {
		// TODO Auto-generated method stub
		char c = e.getKeyChar();
		if (!((Character.isDigit(c) || (c == KeyEvent.VK_DELETE)))) {
			Toolkit.getDefaultToolkit().beep(); // BEEP  || \007
		
			e.consume();
		}
	}

}
